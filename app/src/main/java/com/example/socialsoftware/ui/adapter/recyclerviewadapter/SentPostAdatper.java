package com.example.socialsoftware.ui.adapter.recyclerviewadapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.model.InstaSentPostRV;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.activities.SinglePostViewActivity;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.NetworkUtils.*;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.utils.UiUtils.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.socialsoftware.network.APIConstants.Params.SUCCESS;


public class SentPostAdatper extends RecyclerView.Adapter<SentPostAdatper.ViewHolder>{
    private SinglePostViewActivity listner;
    private LayoutInflater layoutinflate;
    private Context context;
    private ArrayList<InstaSentPostRV> sentPostRV;
    int postid;
    PrefUtils prefUtils;
    PostIdInterface postIdInterface;
    APIInterface apiInterface;
    public SentPostAdatper(PostIdInterface postIdInterface) {
        this.postIdInterface= postIdInterface;
    }
    public interface PostIdInterface{
        public int singlePostId(int POST_ID);
    }

    public SentPostAdatper(Context context, ArrayList<InstaSentPostRV> sentPostRV){
        this.context=context;
        this.sentPostRV=sentPostRV;
        layoutinflate= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view=layoutinflate.inflate(R.layout.insta_sentpost_rv,parent,false);
        ViewHolder viewHolderinflate= new ViewHolder(view);
        apiInterface= APIClient.getClient().create(APIInterface.class);
        return viewHolderinflate;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final InstaSentPostRV instaSentPostRV= sentPostRV.get(position);

        holder.postimageid.setText(instaSentPostRV.getCreatedAt());
        holder.sentPostDescription.setText(instaSentPostRV.getDescription());
        holder.sentPostStatus.setText(instaSentPostRV.getPublish());
        if (holder.sentPostStatus.getText().toString().equals("published")){
            holder.statusPublish.setVisibility(View.VISIBLE);
            holder.statusUnpublish.setVisibility(View.INVISIBLE);
        }
        else {
            holder.statusUnpublish.setVisibility(View.VISIBLE);
            holder.statusPublish.setVisibility(View.INVISIBLE);
        }
        if (instaSentPostRV.getPostimage()==null){
            holder.sentpostimage.setVisibility(View.INVISIBLE);
        }else {
            holder.sentpostimage.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(instaSentPostRV.getPostimage())
                    .into(holder.sentpostimage);
        }

        holder.postViewCard.setOnClickListener(view -> {
            callSinglePostViewactivity(instaSentPostRV.getPostid());

        });
        holder.sentPostMoreButton.setClickable(true);
        holder.sentPostMoreButton.setOnClickListener(view -> {
            Dialog dialogbox = new Dialog(context,WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialogbox.setContentView(R.layout.dialog_posts_option);
            dialogbox.getWindow().setGravity(Gravity.BOTTOM);
            dialogbox.setCancelable(true);
            dialogbox.getWindow().setLayout(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.WRAP_CONTENT);

            TextView viewpost= dialogbox.findViewById(R.id.viewPost);
            TextView deletepost = dialogbox.findViewById(R.id.deletePost);

            viewpost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    callSinglePostViewactivity(instaSentPostRV.getPostid());

                    dialogbox.dismiss();
                }
            });

            deletepost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int cardPosition = holder.getAdapterPosition();
                    deletethePost(instaSentPostRV.getPostid(),cardPosition);
                    dialogbox.dismiss();
                }
            });
            dialogbox.show();
        });

    }

    private void callSinglePostViewactivity(int postid) {
        Intent intent= new Intent(context, SinglePostViewActivity.class);
        intent.putExtra(SinglePostViewActivity.VIEWPOST_ID, postid);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SinglePostViewActivity.POST_MODE,true);
        context.startActivity(intent);
    }

    private void deletethePost(int postid,int cardPosition) {
        UiUtils.showLoadingDialog(context);
        PrefUtils prefUtils= PrefUtils.getInstance(context);
        Call<String> call= apiInterface.deletePost(
                prefUtils.getIntValue(PrefKeys.USER_ID,0)
                ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,"")
                ,postid);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject deletePostPermnatelyResponse = null;
                try{
                    deletePostPermnatelyResponse= new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }
                if (deletePostPermnatelyResponse != null){
                    if (deletePostPermnatelyResponse.optString(SUCCESS).equals(Constants.TRUE)){
                        UiUtils.hideLoadingDialog();
                        UiUtils.showShortToast(context,deletePostPermnatelyResponse.optString(Params.MESSAGE));
                        sentPostRV.remove(cardPosition);
                        notifyItemRemoved(cardPosition);


                    }else {
                        UiUtils.showShortToast(context,deletePostPermnatelyResponse.optString(Params.ERROR));
                        UiUtils.hideLoadingDialog();
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                NetworkUtils.onApiError(context);

            }
        });
    }

    @Override
    public int getItemCount() {
        return sentPostRV.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.instaSentPostImage)
        ImageView sentpostimage;
        @BindView(R.id.instaSentpostTime)
        TextView postimageid;
        @BindView(R.id.postViewCard)
        CardView postViewCard;
        @BindView(R.id.sentPostStatus)
        TextView sentPostStatus;
        @BindView(R.id.sentPostDescription)
        TextView sentPostDescription;
        @BindView(R.id.statusPublish)
        ImageView statusPublish;
        @BindView(R.id.statusUnpublish)
        ImageView statusUnpublish;
        @BindView(R.id.sentPostMorebtn)
        ImageView sentPostMoreButton;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
