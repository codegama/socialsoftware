package com.example.socialsoftware.ui.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.socialsoftware.R;
import com.example.socialsoftware.model.InstaPastRemainderRV;
import com.example.socialsoftware.ui.adapter.recyclerviewadapter.PastRemainderAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PastReminderFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.insta_pastremaider_rv)
    RecyclerView instapastremainderrv;
    @BindView(R.id.nothing)
    TextView nothinghere;
    private LinearLayoutManager linearLayoutManager;
    PastRemainderAdapter pastRemainderAdapter;
    ArrayList<InstaPastRemainderRV> arrayList=new ArrayList<>();


    public PastReminderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_past_reminder, container, false);
        unbinder= ButterKnife.bind(this,view);
        linearLayoutManager=new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false);
        instapastremainderrv.setAdapter(pastRemainderAdapter);
        instapastremainderrv.setLayoutManager(linearLayoutManager);
        return  view;

    }



}
