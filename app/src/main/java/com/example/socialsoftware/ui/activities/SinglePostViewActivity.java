package com.example.socialsoftware.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.model.Platforms;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.adapter.recyclerviewadapter.SentPostAdatper;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.socialsoftware.network.APIConstants.Params.DATA;
import static com.example.socialsoftware.network.APIConstants.Params.PLATFORM;
import static com.example.socialsoftware.network.APIConstants.Params.SUCCESS;
import static com.example.socialsoftware.utils.SharedPref.PrefKeys.DESCRIPTION;
import static com.example.socialsoftware.utils.SharedPref.PrefKeys.PICTURE;

public class SinglePostViewActivity extends BaseActivity implements SentPostAdatper.PostIdInterface {

    @BindView(R.id.singlePostToolBar)
    Toolbar singlePostToolBar;
    @BindView(R.id.singlePostImage)
    ImageView singlePostImage;
    @BindView(R.id.singlePostPlatform)
    TextView singlePostPlatfrom;
    @BindView(R.id.singlePostDate)
    TextView singlePostDate;
    @BindView(R.id.singlePostStatus)
    TextView singlePostStatus;
    @BindView(R.id.singlePostDescrip)
    TextView singlePostDescrip;
    @BindView(R.id.singlePostDelete)
    ImageView singlePostDelete;


    APIInterface apiInterface;
    private  int postid;
    private int postId;
    private boolean postmode;
    public static final String VIEWPOST_ID = "post_id";
    public static final String POST_MODE = "post_mode";
    PrefUtils prefUtils= PrefUtils.getInstance(this);
    private ArrayList<Platforms> platforms= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post_view);
        ButterKnife.bind(this);
        
        getSupportActionBar().hide();
        singlePostToolBar.setNavigationOnClickListener(view -> onBackPressed());



        Intent caller = getIntent();
        int postId = (int) caller.getIntExtra(VIEWPOST_ID, postid);
        Log.i("Post id:", String.valueOf(postId));
        boolean postMode = caller.getBooleanExtra(POST_MODE,postmode);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        getSinglePost(postId);

        singlePostDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteSinglePost(postId);
            }
        });

    }
    private void getSinglePost(int postId) {
        UiUtils.showLoadingDialog(this);
            Call<String> call= (Call<String>) apiInterface.postView(
                    prefUtils.getIntValue(PrefKeys.USER_ID,0),
                    prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,""),
                    postId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()){
                        JSONObject postViewResponse = null;
                        try{
                            postViewResponse= new JSONObject(response.body());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        if (postViewResponse!= null){
                            if (postViewResponse.optString(SUCCESS).equals(Constants.TRUE)){
                                UiUtils.showShortToast(SinglePostViewActivity.this,postViewResponse.optString(Params.MESSAGE));
                                UiUtils.hideLoadingDialog();
                                JSONObject data= postViewResponse.optJSONObject(DATA);
                                showSinglePost(data);
                                JSONArray platformslist = data.optJSONArray("platforms");
                                Log.i("platforms", String.valueOf(platformslist));
                                platforms.clear();
                                if (platformslist.length()!= 0){
                                    for (int i = 0; i < platformslist.length(); i++){
                                        try{
                                            JSONObject listOfPlatform = platformslist.getJSONObject(i);
                                            platforms.add(new Platforms(listOfPlatform.getString(PLATFORM)));
                                            singlePostPlatfrom.setText(platforms.get(0).getPlatform());

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }
                            }else{
                                UiUtils.showShortToast(SinglePostViewActivity.this, postViewResponse.optString(Params.ERROR));
                            }
                        }
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    NetworkUtils.onApiError(SinglePostViewActivity.this);
                }
            });
    }
    private void deleteSinglePost(int postid){
            Call<String> call= apiInterface.deletePost(
                    prefUtils.getIntValue(PrefKeys.USER_ID,0)
                    ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,"")
                    ,postid);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    JSONObject deletePostResponse = null;
                    try{
                        deletePostResponse= new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    if (deletePostResponse != null){
                        if (deletePostResponse.optString(SUCCESS).equals(APIConstants.Constants.TRUE)){
                            UiUtils.showShortToast(SinglePostViewActivity.this,deletePostResponse.optString(Params.MESSAGE));
                            JSONObject data= deletePostResponse.optJSONObject(DATA);
                            //showSinglePost(data);
                            finish();
                        }else {
                            UiUtils.showShortToast(SinglePostViewActivity.this,deletePostResponse.optString(Params.ERROR));
                        }
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    NetworkUtils.onApiError(SinglePostViewActivity.this);

                }
            });
    }

    //showing the post
    private void showSinglePost(JSONObject data) {
       singlePostToolBar.setTitle(getString(R.string.hash_tag)+data.optString(PrefKeys.POST_ID));
       singlePostStatus.setText(data.optString(PrefKeys.STATUS));
       singlePostDescrip.setText(data.optString(PrefKeys.DESCRIPTION));
        Glide.with(SinglePostViewActivity.this)
                .load(data.optString(PICTURE)).into(singlePostImage);

    }

    @Override
    public int singlePostId(int POST_ID) {
        postid = POST_ID;
        return postid;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
