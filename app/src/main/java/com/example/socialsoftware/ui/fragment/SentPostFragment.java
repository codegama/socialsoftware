package com.example.socialsoftware.ui.fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.socialsoftware.R;
import com.example.socialsoftware.model.InstaSentPostRV;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.activities.MainActivity;
import com.example.socialsoftware.ui.adapter.recyclerviewadapter.SentPostAdatper;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SentPostFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.insta_sentpostRV)
    RecyclerView insta_sentpostRV;
    @BindView(R.id.nothinghere)
    TextView nothinghere;

    private SentPostAdatper sentPostAdatper;
    private APIInterface apiInterface;
    MainActivity activity;
    private ArrayList<InstaSentPostRV> instaListPost=new ArrayList<>();

    public SentPostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sent_post, container, false);
        unbinder= ButterKnife.bind(this,view);

        nothinghere.setVisibility(View.INVISIBLE);
        sentPostAdatper= new SentPostAdatper(getContext(),instaListPost);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        insta_sentpostRV.setLayoutManager(linearLayoutManager);
        insta_sentpostRV.setHasFixedSize(true);
        insta_sentpostRV.setAdapter(sentPostAdatper);
        listofPostposted();
        return  view;
    }
    @Override
    public void onResume() {
        super.onResume();
       // getListOfPost();
    }

    private void getListOfPost() {


    }


    private  void listofPostposted(){
        apiInterface= APIClient.getClient().create(APIInterface.class);
        UiUtils.showLoadingDialog(getContext());
        PrefUtils prefUtils = PrefUtils.getInstance(getContext());
        Call<String> call= apiInterface.listPost(
                prefUtils.getIntValue(PrefKeys.USER_ID,0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (isAdded()){
                    if (response.isSuccessful()){
                        JSONObject listPostResponse = null;
                        try{
                            listPostResponse = new JSONObject(response.body());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        if(listPostResponse != null){
                            if (listPostResponse.optString(APIConstants.Params.SUCCESS).equals(APIConstants.Constants.TRUE)){
                                UiUtils.hideLoadingDialog();
                                JSONArray data= listPostResponse.optJSONArray(APIConstants.Params.DATA);
                                instaListPost.clear();
                                if (data.length()!=0){
                                    for (int i = 0; i < data.length(); i++){
                                        try{
                                            JSONObject listOfPost = data.getJSONObject(i);
                                            Log.i("Post", String.valueOf(listOfPost));
                                            instaListPost.add(new InstaSentPostRV(listOfPost.optInt(APIConstants.Params.POST_ID)
                                                    ,listOfPost.optInt(APIConstants.Params.POST_ACCOUNT_ID)
                                                    ,listOfPost.optString(APIConstants.Params.PLATFORM)
                                                    ,listOfPost.optInt(APIConstants.Params.POST_GALLARIES)
                                                    ,listOfPost.optString(APIConstants.Params.PICTURE)
                                                    ,listOfPost.optString(APIConstants.Params.STATUS)
                                                    ,listOfPost.optString(APIConstants.Params.DESCRIPTION)
                                                    ,listOfPost.optString(APIConstants.Params.CREATED_AT)
                                            ));
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                    sentPostAdatper.notifyDataSetChanged();
                                }else {
                                    nothinghere.setVisibility(View.VISIBLE);
                                }
                            } else{
                                UiUtils.showShortToast(getContext(), listPostResponse.optString(APIConstants.Params.ERROR));
                                UiUtils.hideLoadingDialog();
                            }
                        }
                    }
                    insta_sentpostRV.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                NetworkUtils.onApiError(getContext());
            }
        });
    }

}
