package com.example.socialsoftware.ui.adapter.recyclerviewadapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.model.PageAccounts;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;

import java.lang.reflect.Array;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PagesAccountsAdapter extends RecyclerView.Adapter<PagesAccountsAdapter.ViewHolder> {

    LayoutInflater layoutInflater;
    private Context context;
    private  int selectedItem = -1;
    private ArrayList<PageAccounts> pageAccountsArrayList;
    Dialog dialog;
    public PagesAccountsAdapter(Context context, ArrayList<PageAccounts> pageAccountsArrayList) {
        this.context=context;
        this.pageAccountsArrayList=pageAccountsArrayList;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= layoutInflater.inflate(R.layout.pageslist_accounts,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final PageAccounts pageAccounts=pageAccountsArrayList.get(position);
        holder.navHeaderAccount.setBackgroundColor(R.color.white);
        holder.navHeaderAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedItem = pageAccounts.getUserAccountId();
                if(selectedItem == pageAccounts.getUserAccountId()){
                    PrefHelper.removeUserAccountId(context);
                    PrefHelper.setUserAccountId(context,pageAccounts.getUserAccountId());
                    holder.navHeaderAccount.setBackgroundColor(R.color.blue);
                }
                else {
                    if (selectedItem!= pageAccounts.getUserAccountId()){
                        holder.navHeaderAccount.setBackgroundColor(R.color.white);
                    }

                }
              /* selectedItem = pageAccounts.getPageId();
                if(selectedItem==-1){
                    holder.navHeaderAccount.setBackgroundColor(R.color.white);
                }
                else {
                    if(selectedItem != pageAccounts.getPageId()){
                        holder.navHeaderAccount.setBackgroundColor(R.color.white);
                    } else {
                        holder.navHeaderAccount.setBackgroundColor(R.color.blue);
                    }

                }*/
            }
        });
        for (position = 0;position < 5;position++){
            holder.navPageName.setText(pageAccounts.getPagesName());
            holder.navPageTypeName.setText(pageAccounts.getPagesTypePlatform());
            Glide.with(context).load(pageAccounts.getPagesImage()).into(holder.navPageProfileImage);
            //Glide.with(context).load(pageAccounts.getPageTypeImage()).into(holder.navPageTypeImage);
           // holder.navHeaderAccount.setBackgroundColor(pageAccounts.isSelected() ? Color.blue : Color.white);
        }

    }


    @Override
    public int getItemCount() {
        return pageAccountsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.navPageName)
        TextView navPageName;
        @BindView(R.id.navPageProfileImage)
        CircleImageView navPageProfileImage;
        @BindView(R.id.navPageTypeimage)
        CircleImageView navPageTypeImage;
        @BindView(R.id.navPageTypeName)
        TextView navPageTypeName;
        @BindView(R.id.navHeaderAccount)
        View navHeaderAccount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }
}
