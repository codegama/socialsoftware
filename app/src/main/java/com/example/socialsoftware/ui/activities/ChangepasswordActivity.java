package com.example.socialsoftware.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangepasswordActivity extends BaseActivity {
    @BindView(R.id.currentpassword)
    EditText currentpassword;
    @BindView(R.id.newpassword)
    EditText newpassword;
    @BindView(R.id.confirmpassword)
    EditText confirmpassword;
    @BindView(R.id.submitpassword)
    Button submitpassword;
    @BindView(R.id.passwordtoolbar)
    Toolbar passwordtoolbar;

    APIInterface apiInterface;
    AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        ButterKnife.bind(this);

        apiInterface= APIClient.getClient().create(APIInterface.class);


        passwordtoolbar.setTitle(getString(R.string.change_password));
        getSupportActionBar().hide();
        passwordtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

    }
    @OnClick({R.id.submitpassword})
    public void OnViewClicked(View view)
    {
        switch (view.getId()){
            case  R.id.submitpassword:
                if (validatepasswordfield()){
                    dialog= new AlertDialog.Builder(this);
                    dialog.setTitle(getString(R.string.are_you_sure_to_change_passowrd))
                            .setPositiveButton(getString(R.string.yes),(dialogInterface, i) -> {
                                dialogInterface.dismiss();
                                changepassword();
                            })
                            .setNegativeButton(getString(R.string.no),(dialogInterface, i) -> {
                                dialogInterface.cancel();
                            }).show();
                }
                break;
        }

    }

    private boolean validatepasswordfield() {
       if(currentpassword.getText().toString().trim().length()==0 ){
            Toast.makeText(this, getString(R.string.current_password_cannot_be_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if(newpassword.getText().toString().trim().length()==0 ){
            Toast.makeText(this,getString(R.string.new_password_canot_be_empty), Toast.LENGTH_SHORT).show();
           return false;
        }
        if (confirmpassword.getText().toString().trim().length()==0) {
            Toast.makeText(this, getString(R.string.confirm_password_cannot_be_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!newpassword.getText().toString().equals(confirmpassword.getText().toString())){
            Toast.makeText(this, getString(R.string.new_password_and_confirm_password_should_same), Toast.LENGTH_SHORT).show();
           return false;
        }
        return  true;
    }

    private void changepassword() {
        PrefUtils prefUtils = PrefUtils.getInstance(getApplicationContext());
        Call<String> call=apiInterface.changeuserpassowrd(
                prefUtils.getIntValue(PrefKeys.USER_ID, -1)
                , prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""),
                currentpassword.getText().toString(),
                newpassword.getText().toString(),
                confirmpassword.getText().toString());

        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject changePasswordResponse=null;
                try{
                    changePasswordResponse=new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (changePasswordResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                    UiUtils.showShortToast(ChangepasswordActivity.this, changePasswordResponse.optString(Params.MESSAGE));
                    logOutUserInDevice();
                }
                else {
                    UiUtils.showShortToast(ChangepasswordActivity.this, changePasswordResponse.optString(Params.ERROR));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(ChangepasswordActivity.this);
                clearallfield();
            }
        });

    }

    private void clearallfield() {
        newpassword.setText("");
        currentpassword.setText("");
        confirmpassword.setText("");
    }

    private void logOutUserInDevice() {
        PrefHelper.setUserLoggedOut(this);
        Intent restartApp = new Intent(this, LoginActivity.class);
        restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(restartApp);
        this.finish();
    }
}
