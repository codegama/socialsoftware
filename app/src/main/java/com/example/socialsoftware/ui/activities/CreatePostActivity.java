package com.example.socialsoftware.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.socialsoftware.R;
import com.example.socialsoftware.model.AccountsProfileImage;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.adapter.recyclerviewadapter.AccountsaddAdapter;
import com.example.socialsoftware.ui.fragment.QueuePostFragment;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePostActivity extends BaseActivity {
    @BindView(R.id.shareaccountsImage)
    CircleImageView shareaccountsImage;
    @BindView(R.id.sharePostTitle)
    TextView sharePostTitle;
    @BindView(R.id.sharefabaddphotos)
    View sharefabaddphotos;
    @BindView(R.id.sharetomedia)
    TextView sharetomedia;
    @BindView(R.id.selected_shareimage)
    ImageView selectedshareimage;
    @BindView(R.id.shareimage_cancel)
    Button shareimagecancel;
    @BindView(R.id.shareimage_edit)
    Button shareimageedit;
    @BindView(R.id.layout_shareimage)
    View layoutshareimage;

    AccountsaddAdapter accountsaddAdapter;
    APIInterface apiInterface;
    private  Uri postImage= null;
    private  File cropPostImage;
    PrefUtils prefUtils = PrefUtils.getInstance(this);

    private int GALLERY = 1, CAMERA = 2;
    public  static final int RequestPermissionCode  = 1;
    static Uri file;

   private ArrayList<AccountsProfileImage> arrayList;
   private  String cameraFilePath= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        apiInterface= APIClient.getClient().create(APIInterface.class);

        Glide.with(this).load(prefUtils.getStringValue(PrefKeys.USER_PICTURE,"")).into(shareaccountsImage);
        layoutshareimage.setVisibility(View.GONE);
    }
    @OnClick({R.id.sharefabaddphotos,R.id.sharetomedia,R.id.shareimage_edit,R.id.shareimage_cancel,R.id.selected_shareimage})
    public void OnViewClick(View view){
        switch (view.getId()){
            case R.id.sharefabaddphotos:
                addPhototoShare();
                break;
            case R.id.shareimage_cancel:
                layoutshareimage.setVisibility(View.GONE);
                sharefabaddphotos.setVisibility(View.VISIBLE);
                break;
            case R.id.sharetomedia:
                if (validatecontent()){
                    sharetoSocialMedia(postImage);
                }
                else {
                    AlertDialog.Builder builder= new AlertDialog.Builder(this);
                    builder.setTitle(getString(R.string.uh_oh))
                            .setMessage(getString(R.string.we_would_like_to_post_this_but))
                            .setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).show();
                }
                break;
        }
    }
    private boolean validatecontent() {
        if (sharePostTitle.getText().toString().length() == 0 && selectedshareimage.getDrawable() == null){
            return false;
        }
        return  true;
    }
    //Share to Social Media
    private void sharetoSocialMedia(Uri postImageTO) {
        UiUtils.showLoadingDialog(this);
        MultipartBody.Part multipartBody = null;
        if (postImageTO != null){
            File file = new File(getRealPathFromURIPath(postImageTO, this));

            // create RequestBody instance tempFrom file
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("images/*"), file);

            // MultipartBody.Part is used to send also the actual file name
            multipartBody =
                    MultipartBody.Part.createFormData(Params.PICTURE, file.getName(), requestFile);
        }

        Call<String> call= apiInterface.createPost(
                prefUtils.getIntValue(PrefKeys.USER_ID, 0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""),
                multipartBody,
                Constants.FACEBOOK_LOGIN,
                sharePostTitle.getText().toString()
        ,prefUtils.getIntValue(PrefKeys.USER_ACCOUNT_ID,0));
        call.enqueue(new Callback<String>(){
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    JSONObject createPostResponse = null;
                    try {
                        createPostResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    if (createPostResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                        UiUtils.showShortToast(CreatePostActivity.this,createPostResponse.optString(Params.MESSAGE));
                        JSONObject data= createPostResponse.optJSONObject(Params.DATA);
                        addtoQueue(data);
                        finish();
                    }
                    else {
                        UiUtils.hideLoadingDialog();
                        UiUtils.showShortToast(CreatePostActivity.this, createPostResponse.optString(Params.ERROR));
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                NetworkUtils.onApiError(CreatePostActivity.this);
            }
        });
    }

    private void addtoQueue(JSONObject data) {
        QueuePostFragment fragment= new QueuePostFragment();
        Bundle  bundle=new Bundle();
        if (data.optString(Params.POST_ID)!=null){
            bundle.putInt("CURRENTPOST_ID", data.optInt(Params.POST_ID));
            fragment.setArguments(bundle);
        }

    }

    private String getRealPathFromURIPath(Uri postImage, CreatePostActivity createPostActivity) {
        Cursor cursor = createPostActivity.getContentResolver().query(postImage, null, null, null, null);
        if (cursor == null) {
            return postImage.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void addPhototoShare() {
        Dialog sharedialog=new Dialog(this, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sharedialog.setContentView(R.layout.layout_select_imagetoshare);
        sharedialog.getWindow().setLayout(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.WRAP_CONTENT);
        sharedialog.getWindow().setGravity(Gravity.BOTTOM);

        TextView selectcamera=sharedialog.findViewById(R.id.select_camera);
        TextView selectgalery=sharedialog.findViewById(R.id.select_gallery);
        //camera
        selectcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPhotofromCamera();
                sharedialog.dismiss();
            }
        });
        //gallery
        selectgalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPhotofromGallery();
                sharedialog.dismiss();
            }
        });
        sharedialog.show();
    }

    private void selectPhotofromGallery() {
        try{
            if (ActivityCompat.checkSelfPermission(CreatePostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CreatePostActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY);
        }else {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                galleryIntent.setType("image/*");
                layoutshareimage.setVisibility(View.VISIBLE);
                startActivityForResult(galleryIntent, GALLERY);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void selectPhotofromCamera() {
        try{
            if (ActivityCompat.checkSelfPermission(CreatePostActivity.this,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(CreatePostActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, CAMERA);
            }else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                layoutshareimage.setVisibility(View.VISIBLE);
                startActivityForResult(cameraIntent, CAMERA);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY){
            postImage = data.getData();
            layoutshareimage.setVisibility(View.VISIBLE);
            Glide.with(this).load(postImage).into(selectedshareimage);
            sharefabaddphotos.setVisibility(View.GONE);
        }
        else if (resultCode == RESULT_OK && requestCode == CAMERA){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            Log.i("Bitmap data", String.valueOf(bitmap));
            selectedshareimage.setImageBitmap(bitmap);
            sharefabaddphotos.setVisibility(View.GONE);
        }
    }
}
