package com.example.socialsoftware.ui.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.ui.activities.InitialActivity;
import com.example.socialsoftware.ui.activities.UserProfileviewActivity;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostSettingFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.removeaccount)
    TextView removeaccount;
    @BindView(R.id.profiledetails)
    TextView profiledetails;
    Dialog accountremovedialog;
    APIInterface apiInterface;


    public PostSettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_post_setting, container, false);
        unbinder= ButterKnife.bind(this,view);
        apiInterface= APIClient.getClient().create(APIInterface.class);
        return view;

    }
    @OnClick({R.id.removeaccount,R.id.profiledetails})
    public  void OnViewClicked(View view){
        switch (view.getId()){
            case R.id.removeaccount:
                accountremovedialog= new Dialog(getContext());
                accountremovedialog.setContentView(R.layout.dialog_removeaccount);
                EditText userPassword= accountremovedialog.findViewById(R.id.delete_userpassword);
                Button yes=accountremovedialog.findViewById(R.id.yes_deleteaccount);
                Button no =accountremovedialog.findViewById(R.id.no_deleteaccount);
                yes.setOnClickListener(view12 -> deleteAccount(userPassword.getText().toString().trim()));
                no.setOnClickListener(view1 -> {
                    Toast.makeText(getContext(), getString(R.string.remove_account_canceled), Toast.LENGTH_SHORT).show();
                    accountremovedialog.dismiss();
                });
                accountremovedialog.show();
                break;
            case R.id.profiledetails:
                startActivity(new Intent(getContext(), UserProfileviewActivity.class));
                break;
        }
    }

    private void deleteAccount(String userpassword) {
        PrefUtils prefUtils = PrefUtils.getInstance(getContext());
        Call<String> call= apiInterface.deleteuseraccount(
                prefUtils.getIntValue(PrefKeys.USER_ID, 0)
                ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""),
                userpassword);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject deleteAccountResponse=null;
                try{
                    deleteAccountResponse= new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                if (response.isSuccessful()){
                    if (deleteAccountResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                        UiUtils.showShortToast(getContext(), deleteAccountResponse.optString(Params.MESSAGE));
                        JSONObject data= deleteAccountResponse.optJSONObject(Params.DATA);
                        Log.d("response",response.body());
                        Intent intent=new Intent(getContext(), InitialActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        //getActivity();
                    }
                    else{
                        UiUtils.showShortToast(getContext(), deleteAccountResponse.optString(Params.ERROR_MESSAGE));
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(getContext());

            }
        });

    }


}
