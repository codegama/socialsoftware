package com.example.socialsoftware.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.UiUtils;
import com.example.socialsoftware.utils.Utils;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends BaseActivity {
    @BindView(R.id.signupterms)
    TextView signupterms;
    @BindView(R.id.signupbackbtn)
    ImageButton signupbackbtn;
    @BindView(R.id.layoutsignin)
    TextView layoutsignin;
    @BindView(R.id.signupbtn)
    Button signbtn;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.signupemail)
    EditText signupemail;
    @BindView(R.id.signuppassword)
    EditText signuppassword;
    @BindView(R.id.layoutusername)
    TextInputLayout layoutusername;
    @BindView(R.id.layoutsignupemail)
    TextInputLayout layoutemail;
    @BindView(R.id.layoutsignuppassword)
    TextInputLayout layoutsignuppassword;


    APIInterface apiInterface;
    private String spanstring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        getSupportActionBar().hide();
        apiInterface = APIClient.getClient().create(APIInterface.class);

        spannedstring();
    }

    private void spannedstring() {
        spanstring=(String)signupterms.getText();
        Spannable WordtoSpanstring = new SpannableString(spanstring);
        WordtoSpanstring.setSpan(new ForegroundColorSpan(Color.BLUE), 38, 55,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        signupterms.setText(WordtoSpanstring);

    }
    @OnClick({R.id.layoutsignin,R.id.signupbackbtn,R.id.signupbtn,R.id.signupterms})
    public void onViewClicked(View view){
        switch (view.getId()){
            case R.id.signupbackbtn:
                startActivity(new Intent(this,InitialActivity.class));
                finish();
                break;
            case R.id.layoutsignin:
                startActivity(new Intent(this,LoginActivity.class));
                finish();
                break;
            case R.id.signupbtn:
                if (Validatedfield()){
                    registeruser();
                }
                break;
            case R.id.signupterms:
                Intent intent=new Intent(this,StaticPageViewActivity.class);
                intent.putExtra(StaticPageViewActivity.PAGE_TYPE,StaticPageViewActivity.PageTypes.TERMS);
                startActivity(intent);
                break;
        }
    }

    private void registeruser() {
        UiUtils.showLoadingDialog(this);
        Call<String> call = apiInterface.register(username.getText().toString()
                , signupemail.getText().toString()
                , signuppassword.getText().toString()
                , Constants.MANUAL_LOGIN
                , Constants.ANDROID
                , NetworkUtils.getDeviceToken(this));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject signupResponse = null;
                try {
                    signupResponse = new JSONObject(response.body());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful()){
                    if (signupResponse != null) {
                        if (signupResponse.optString(Params.SUCCESS).equals(Constants.TRUE)) {
                            UiUtils.showShortToast(SignupActivity.this, signupResponse.optString(Params.MESSAGE));
                            JSONObject data= signupResponse.optJSONObject(Params.DATA);
                            loginUserInDevice(data,Constants.MANUAL_LOGIN);
                        } else {
                            UiUtils.showShortToast(SignupActivity.this, signupResponse.optString(Params.ERROR));
                            Intent mainActivity = new Intent(SignupActivity.this, SignupActivity.class);
                            mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(mainActivity);
                            finish();
                            if (!signupResponse.optString(Params.ERROR).contains("taken")) {
                                clearAllFields();
                            }
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(SignupActivity.this);
            }
        });
    }

    private void clearAllFields() {
        username.setText("");
        signupemail.setText("" );
        signuppassword.setText("");
    }

    private boolean Validatedfield() {
        if (username.getText().toString().trim().length() == 0) {
            UiUtils.showShortToast(this,getString(R.string.name_cannot_be_empty));
            return false;
        }
        if (signupemail.getText().toString().trim().length() == 0) {
            UiUtils.showShortToast(this,getString(R.string.email_cant_be_empty));
            return false;
        }
        if (!Utils.isValidEmail(signupemail.getText().toString())){
            UiUtils.showShortToast(this,getString(R.string.please_enter_valid_email));
            return false;
        }

        if (signuppassword.getText().toString().trim().length() == 0) {
            UiUtils.showShortToast(this,getString(R.string.password_cant_be_empty));
            return false;
        }

        if (signuppassword.getText().toString().trim().length() < 6) {
            UiUtils.showShortToast(this,getString(R.string.password_cannot_be_less_than_six_character));
            return false;
        }
        return true;
    }
    private void loginUserInDevice(JSONObject data, String loginBy) {
        PrefHelper.setUserLoggedIn(this, data.optInt(Params.USER_ID)
                , data.optString(Params.TOKEN)
                , loginBy
                , data.optString(Params.EMAIL)
                , data.optString(Params.NAME)
                , data.optString(Params.MOBILE)
                , data.optString(Params.DESCRIPTION)
                , data.optString(Params.PICTURE)
                , data.optString(Params.NOTIF_PUSH_STATUS)
                , data.optString(Params.NOTIF_PUSH_STATUS)
                ,data.optInt(Params.IS_SOCIAL_ACCOUNT_CONNECTED));
        Intent mainActivity = new Intent(this, MainActivity.class);
        mainActivity.putExtra(MainActivity.FIRST_TIME,  true);
        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainActivity);
        this.finish();
    }

}
