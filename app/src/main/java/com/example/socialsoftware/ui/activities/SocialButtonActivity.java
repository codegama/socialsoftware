package com.example.socialsoftware.ui.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.SharedPref.PrefHelper;
import com.example.socialsoftware.utils.SharedPref.PrefKeys;
import com.example.socialsoftware.utils.SharedPref.PrefUtils;
import com.example.socialsoftware.utils.UiUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.facebook.FacebookSdk;

public class SocialButtonActivity extends BaseActivity {
    public static final String FIRST_TIME ="firsttime" ;
    public static final String SHOW_MORE ="showmore";
    public static final String ISSOCIALACCOUNTS = "issocialaccounts";
    @BindView(R.id.socialbackbtn)
    ImageButton socialbackbtn;
    @BindView(R.id.facebooklogin_button)
    Button facebookloginbtn;
    @BindView(R.id.termsandcond)
    TextView termsandcondition;
    @BindView(R.id.whyThis)
    TextView whythis;
    @BindView(R.id.socialOption)
    ImageView socialmoreOption;

    CallbackManager callbackManager;
    APIInterface apiInterface;
    private static final int RC_SIGN_IN = 100;
    String spaningstring;
    PrefUtils prefUtils;
    private  boolean showmore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_button);
        ButterKnife.bind(this);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        spaningstring();
        getSupportActionBar().hide();

        Intent caller = getIntent();

        boolean showMore = caller.getBooleanExtra(SHOW_MORE,showmore);

        if (showMore){
            socialmoreOption.setVisibility(View.VISIBLE);
        }else {
            socialmoreOption.setVisibility(View.INVISIBLE);
        }

        callbackManager = CallbackManager.Factory.create();
        logoutfromFacebook();


    }
    private void spaningstring() {
        spaningstring=(String)termsandcondition.getText();
        Spannable WordtoSpanstring = new SpannableString(spaningstring);
        WordtoSpanstring.setSpan(new ForegroundColorSpan(Color.BLUE), 38, 55,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        termsandcondition.setText(WordtoSpanstring);

    }

    private void logoutfromFacebook() {
            if (AccessToken.getCurrentAccessToken() == null) {
                return; // already logged out
            }
            new GraphRequest(AccessToken.getCurrentAccessToken(),
                    "/me/permissions/", null, HttpMethod.DELETE, graphResponse -> LoginManager.getInstance().logOut()).executeAsync();

    }

    @OnClick({R.id.socialbackbtn,R.id.facebooklogin_button,R.id.termsandcond,R.id.whyThis,R.id.socialOption})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.socialbackbtn:
                onBackPressed();
                break;
            case R.id.facebooklogin_button:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                LoginManager.getInstance().logInWithPublishPermissions(this,Arrays.asList("manage_pages","publish_pages"));
                LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (AccessToken.getCurrentAccessToken() != null) {

                            /*GraphRequest request=GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                String fb_id;
                                String name;
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    Log.i("Facebook Login", String.valueOf(response));
                                    try {
                                        fb_id = object.getString("id");
                                        name =object.getString("name");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    GraphRequest pagerequest = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "/me/accounts",new GraphRequest.Callback(){
                                        @Override
                                        public void onCompleted(GraphResponse response) {
                                            Log.d("Response", String.valueOf(response));
                                        }
                                    });
                                    pagerequest.executeAsync();
                                }
                            });*/
                           GraphRequest pagerequest = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(),  "/me/accounts",new GraphRequest.Callback(){
                                @Override
                                public void onCompleted(GraphResponse response) {
                                    try {
                                        JSONObject facebookresponse= new JSONObject(response.getRawResponse());
                                        Log.d("FacebookResponse", String.valueOf(facebookresponse));
                                        JSONArray accountdata = facebookresponse.optJSONArray("data");
                                        //Account Data JSONObject
                                        JSONObject jsonObject = new JSONObject();
                                        String id = null;
                                        for (int i=0;i<accountdata.length();i++){
                                            JSONObject data= accountdata.optJSONObject(i);
                                            id = data.optString("id");
                                            jsonObject.put("access_token",data.optString("access_token"));
                                            jsonObject.put("category",data.optString("category"));
                                            jsonObject.put("name",data.optString("name"));
                                            jsonObject.put("id",data.optString("id"));
                                        }
                                        URL picture = null;
                                        picture = new URL("http://graph.facebook.com/"+id+"/picture");
                                        jsonObject.put("picture",picture);
                                        Log.i("pixcture", String.valueOf(picture));
                                        //Account data JSONArray
                                        JSONArray pagedata =  new JSONArray();
                                        pagedata.put(jsonObject);
                                        Log.d("Account Data", String.valueOf(pagedata));
                                        pagedata.put(jsonObject);
                                        //CALL createPage Account using JSON Array (AccessToken , category , name , id, picture)
                                        String accountData = String.valueOf(pagedata);
                                        createuserPageAccount(Constants.FACEBOOK_LOGIN, accountData);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            });
                           pagerequest.executeAsync();
                           //request.executeAsync();
                        }
                    }
                    @Override
                    public void onCancel() {
                        LoginManager.getInstance().logOut();
                        finish();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        UiUtils.showShortToast(SocialButtonActivity.this, getString(R.string.something_went_wrong));
                        //AccessToken.setCurrentAccessToken(null);
                    }
                });
                break;
            case R.id.termsandcond:
               Intent intent=new Intent(this,StaticPageViewActivity.class);
               intent.putExtra(StaticPageViewActivity.PAGE_TYPE,StaticPageViewActivity.PageTypes.TERMS);
               startActivity(intent);
               break;
            case R.id.whyThis:
                showWhyThisdialog();
                break;
            case R.id.socialOption:
                showSignoutoption();
                break;

        }
    }
    private void showSignoutoption() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.signing_out))
                .setMessage(getString(R.string.you_want_signout_form_application_this_will_stop_the_remainder_from_the_accounts))
                .setPositiveButton(getString(R.string.yes ), (dialogInterface, i) -> {
                    signOutfromApp();
                })
                .setNegativeButton(getString(R.string.no), (dialogInterface,i)->{
                    Toast.makeText(getApplicationContext(),getString( R.string.cancelled_the_signout),Toast.LENGTH_SHORT).show();
                }).show();

    }

    private void signOutfromApp() {
            prefUtils=PrefUtils.getInstance(this);
            Call<String> call =apiInterface.logOutUser(
                    prefUtils.getIntValue(PrefKeys.USER_ID,0),
                    prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    JSONObject signOutResponse = null;
                    try{
                        signOutResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if (signOutResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                        UiUtils.showShortToast(SocialButtonActivity.this, signOutResponse.optString(Params.MESSAGE));
                        logOutUserInDevice();
                    }
                    else {
                        UiUtils.showShortToast(SocialButtonActivity.this,signOutResponse.optString(Params.ERROR));
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    NetworkUtils.onApiError(SocialButtonActivity.this);
                }
            });
    }

    private void logOutUserInDevice() {
        PrefHelper.setUserLoggedOut(this);
        Intent logagain= new Intent(this, InitialActivity.class);
        logagain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(logagain);
        this.finish();
    }

    private void showWhyThisdialog() {
        Dialog dialogBox = new Dialog(this, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialogBox.setContentView(R.layout.dialog_whythis);
        dialogBox.getWindow().setGravity(Gravity.CENTER);
        dialogBox.setCancelable(true);
        dialogBox.getWindow().setLayout(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.WRAP_CONTENT);

        TextView ok = dialogBox.findViewById(R.id.okconnect);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBox.dismiss();
            }
        });
        dialogBox.show();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    protected  void createuserPageAccount(String platform, String accountdata){
        PrefUtils prefUtils = PrefUtils.getInstance(this);
        Call<String> call = apiInterface.createUserAccount(
                prefUtils.getIntValue(PrefKeys.USER_ID,0)
                ,prefUtils.getStringValue(PrefKeys.SESSION_TOKEN,"")
                ,platform
                ,accountdata);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    JSONObject createAccountResponse = null;
                    try{
                        createAccountResponse = new JSONObject(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    if (createAccountResponse!=null){
                        if (createAccountResponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                            UiUtils.showShortToast(SocialButtonActivity.this,createAccountResponse.optString(Params.MESSAGE));
                            JSONArray succesData = createAccountResponse.optJSONArray(Params.DATA);
                            JSONObject data = new JSONObject();
                            int userAccountId = 0 ;
                            for (int i =0; i < succesData.length();i++){
                                data = succesData.optJSONObject(i);
                                Log.i("Data", String.valueOf(data));
                                userAccountId = data.optInt(Params.USERACCOUNT_ID);
                                Log.i("Data", String.valueOf(userAccountId));
                            }
                            Intent toMain = new Intent(SocialButtonActivity.this,MainActivity.class);
                            toMain.putExtra(SocialButtonActivity.ISSOCIALACCOUNTS,userAccountId);
                            startActivity(toMain);
                            finish();
                        }else {
                            UiUtils.showShortToast(SocialButtonActivity.this,createAccountResponse.optString(Params.ERROR));
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                UiUtils.hideLoadingDialog();
                NetworkUtils.onApiError(SocialButtonActivity.this);
            }
        });
    }

     /*private void loginUserInDevice(JSONObject data, String login_by) {
        PrefHelper.setUserLoggedIn(this, data.optInt(Params.USER_ID)
                , data.optString(Params.TOKEN)
                , login_by
                , data.optString(Params.EMAIL)
                , data.optString(Params.NAME)
                , data.optString(Params.MOBILE)
                , data.optString(Params.DESCRIPTION)
                , data.optString(Params.PICTURE)
                , data.optString(Params.NOTIF_PUSH_STATUS)
                , data.optString(Params.NOTIF_PUSH_STATUS));
        Intent toMain = new Intent(this, MainActivity.class);
        toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(toMain);
        this.finish();
    }*/
}
