package com.example.socialsoftware.ui.adapter.recyclerviewadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.socialsoftware.R;
import com.example.socialsoftware.model.InstaPastRemainderRV;

import java.util.ArrayList;

public class PastRemainderAdapter extends RecyclerView.Adapter<PastRemainderAdapter.ViewHolder> {
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<InstaPastRemainderRV> arrayList;

    public PastRemainderAdapter(Context context, ArrayList<InstaPastRemainderRV> arrayList){
        this.context=context;
        this.arrayList=arrayList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.insta_pastremainder_rv,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final InstaPastRemainderRV instaPastRemainderRV=arrayList.get(position);
        holder.pastremaintime.setText(instaPastRemainderRV.getPastremaintime());
        holder.remainderimage.setImageResource(instaPastRemainderRV.getRemainderimage());
        holder.remaidertext.setText(instaPastRemainderRV.getRemaidertext());
        holder.repostimage.setImageResource(instaPastRemainderRV.getRepostimage());
        holder.remaiderpostimage.setImageResource(instaPastRemainderRV.getRemaiderpostimage());
        holder.posttoinsta.setText(instaPastRemainderRV.getPosttoinsta());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView remainderimage,remaiderpostimage,repostimage;
        TextView remaidertext,posttoinsta,pastremaintime;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

        }
    }

}
