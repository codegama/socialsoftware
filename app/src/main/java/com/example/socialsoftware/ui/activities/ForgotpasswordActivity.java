package com.example.socialsoftware.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.socialsoftware.R;
import com.example.socialsoftware.network.APIClient;
import com.example.socialsoftware.network.APIConstants.Constants;
import com.example.socialsoftware.network.APIConstants.Params;
import com.example.socialsoftware.network.APIInterface;
import com.example.socialsoftware.utils.NetworkUtils;
import com.example.socialsoftware.utils.UiUtils;
import com.example.socialsoftware.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotpasswordActivity extends BaseActivity {
    @BindView(R.id.forgotpasswordemail)
    EditText forgotpasswordemail;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.forgotpasswordToolbar)
    Toolbar forgotpasswordToolbar;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        ButterKnife.bind(this);

        forgotpasswordToolbar.setTitle(getString(R.string.forgot_password));
        getSupportActionBar().hide();
        forgotpasswordToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        apiInterface= APIClient.getClient().create(APIInterface.class);
    }
    @OnClick(R.id.submit)
    public  void OnViewClicked(View view){
        switch (view.getId()){
            case R.id.submit:
                if (validatefield())
                    sendEmailconfigration();
                break;

        }
    }

    private boolean validatefield() {
        if (forgotpasswordemail.getText().toString().equals("")){
            Toast.makeText(ForgotpasswordActivity.this,getString(R.string.email_cant_be_empty),Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Utils.isValidEmail(forgotpasswordemail.getText().toString())){
            Toast.makeText(ForgotpasswordActivity.this,getString(R.string.please_enter_valid_email),Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void sendEmailconfigration() {
        Call<String> call = apiInterface.forgotuserpassword( forgotpasswordemail.getText().toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject forgotPasswordReponse=null;
                try{
                    forgotPasswordReponse = new JSONObject(response.body());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                if (forgotPasswordReponse.optString(Params.SUCCESS).equals(Constants.TRUE)){
                    UiUtils.showShortToast(ForgotpasswordActivity.this, forgotPasswordReponse.optString(Params.MESSAGE));
                    finish();
                }
                else {
                    UiUtils.showShortToast(ForgotpasswordActivity.this, forgotPasswordReponse.optString(Params.ERROR));
                    clearfield();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                NetworkUtils.onApiError(ForgotpasswordActivity.this);

            }
        });

    }
    private void clearfield() {
        forgotpasswordemail.setText("");
    }
}
