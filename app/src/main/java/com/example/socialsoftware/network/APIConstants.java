package com.example.socialsoftware.network;

import java.security.PublicKey;

public class APIConstants {

    private  APIConstants(){

    }

    //Constants class
    public static class Constants {
        public static final String MANUAL_LOGIN = "manual";
        public static final String DEVICE_TOKEN="123456";
        public static final String GOOGLE_LOGIN = "google";
        public static final String FACEBOOK_LOGIN = "facebook";
        public static final String ANDROID = "android";
        public static final String SUCCESS = "success";
        public static final String ERROR = "error";
        public static final String TRUE = "true";
        public static final String FALSE = "false";
    }

    //URLs class
    static class URLs {
        static final String LIVE_URL = "https://social-share.botfingers.com/";
        //        static final String STAGING_URL = "http://social-share.botfingers.com/";
        static final String BASE_URL = LIVE_URL;

        private URLs() {

        }
    }

    //APIs
    public static class APIs {
        public static final String API_STR = "api/user/";
        public static final String LOGIN = API_STR + "login";
        public static final String REGISTER = API_STR + "register";
        public static final String SOCIAL_LOGIN = REGISTER;
        public static final String FORGOT_PASSWORD=API_STR + "forgot_password";
        public static final String CHANGE_PASSWORD=API_STR + "change_password";
        public static final String LOGOUT = API_STR + "logout";
        public static final String DELETE_ACCOUNT=API_STR + "delete_account";
        public static final String UPDATE_PROFILE=API_STR + "update_profile";
        public static final String USER_PROFILE = API_STR + "profile";
        public static final String LIST_POST = API_STR + "posts";
        public static final String POST_LISTS = API_STR + "posts/";
        public static final String CREATE_POST = POST_LISTS + "create";
        public static final String DELETE_POST = POST_LISTS + "delete";
        public static final String POST_VIEW = POST_LISTS + "view";
        public static final String STATIC_PAGE = "api/";
        public static final String GET_STATIC_PAGE = STATIC_PAGE + "pages/list";
        //Facebook Pages
        public static final String CREATE_USER_ACCOUNT= STATIC_PAGE +"user_accounts_save";
        public static final String DELETE_USER_ACCOUNT = STATIC_PAGE + "user_accounts_delete";
        public static  final String LIST_USER_ACCOUNTS ="api/user_accounts";


        private APIs() {
        }
    }


    //Params
    public static class Params {
        public static final String ID = "id";
        public static final String TOKEN = "token";
        public static final String NAME = "name";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String LOGIN_BY = "login_by";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String DEVICE_TYPE = "device_type";
        public static final String MESSAGE = "message";
        public static final String ERROR_MESSAGE = "error_messages";
        public static final String ERROR = "error";
        public static final String USER_ID = "user_id";
        public static final String SUCCESS = "success";
        public static final String PICTURE = "picture";
        public static final String DESCRIPTION = "description";
        public static final String CREATED_AT ="created_at";
        public static final String OLD_PASSWORD = "old_password";
        public static final String CONFIRM_PASSWORD = "password_confirmation";
        public static final String SOCIAL_UNIQUE_ID = "social_unique_id";
        public static final String ERROR_CODE = "error_code";
        public static final String MOBILE = "mobile";
        public static final String ERROR_MSG = "error_message";
        public static final String DATA = "data";
        public static final String PLATFORMLIST = "platforms";
        public static final String NOTIF_PUSH_STATUS = "push_status";
        public static final String NOTIF_EMAIL_STATUS = "email_notification_status";
        public static final String FIRST_NAME = "firstname";
        public static final String LAST_NAME="lastname";
        public static final String PLATFORM = "platform";
        //list post
        public static final String POST_ID ="post_id" ;
        public static final String POST_ACCOUNT_ID="post_account_id";
        public static final String POST_GALLARIES="post_gallaries_id";

        //Static Page
        public static final String PAGE_TYPE = "page_type";
        public static final String HELP ="help";
        public static final String PRIVACY_POLICY = "privacy";
        public static final String TERMS = "terms";
        public static final String TITLE = "title";
        public static final String ABOUT = "about";
        public static final String SUPPORT = "support";
        public static final String STATUS = "status" ;
        //facebook Pages
        public static final String ACCOUNTDATA = "account_data";
        public static final String USERACCOUNT_ID ="user_account_id" ;
        public static final String PAGE_CATEGORY = "page_category";
        public static final String PAGE_ID = "page_id";
        public static final String PAGE_NAME ="page_name";
        public static final String PAGE_ACCESS_TOKEN ="page_access_token";
        public static final String USER_ACCESS_TOKEN ="user_access_token";
        public static final String IS_SOCIAL_ACCOUNT_CONNECTED = "is_social_account_connected";


        Params(){

        }
    }

    public static class STATIC_PAGES {
        public static final String TERMS = "terms";
        public static final String PRIVACY = "privacy";
        public static final String HELP = "help";
        public static final String ABOUT = "about";

        public static final String ABOUT_URL = "";
        public static final String TERMS_URL = "";
        public static final String HELP_URL = "";
        public static final String SPEED_TEST_URL = "https://fast.com";
        public static final String PRIVACY_URL = "";
    }


    //ERROR class
    public static class ErrorCodes {
        static final int TOKEN_EXPIRED = 1003;
        static final int USER_DOESNT_EXIST = 1002;
        static final int USER_RECORD_DELETED_CONTACT_ADMIN = 3000;
        static final int INVALID_TOKEN = 1004;
        static final int USER_LOGIN_DECLINED = 905;
        static final int EMAIL_NOT_ACTIVATED = 1001;
        static final int EMAIL_ALREADY_EXISTS = 101;
        static final int EMAIL_CONFIGURATION_FAILED = 1006;

        private ErrorCodes() {
        }
    }

}
