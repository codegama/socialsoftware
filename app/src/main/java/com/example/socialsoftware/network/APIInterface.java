package com.example.socialsoftware.network;

import com.example.socialsoftware.network.APIConstants.APIs;
import com.example.socialsoftware.network.APIConstants.Params;

import org.json.JSONArray;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIInterface {

    //LOGIN API
    @FormUrlEncoded
    @POST(APIs.LOGIN)
    Call<String> loginUser(@Field(Params.EMAIL) String email
            , @Field(Params.PASSWORD) String password
            , @Field(Params.LOGIN_BY) String login_by
            , @Field(Params.DEVICE_TYPE) String device_type
            , @Field(Params.DEVICE_TOKEN) String device_token);
    @FormUrlEncoded
    @POST(APIs.SOCIAL_LOGIN)
    Call<String> socialLoginUser(@Field(Params.SOCIAL_UNIQUE_ID) String social_unique_id
            ,@Field(Params.EMAIL) String email
            ,@Field(Params.NAME) String name
            ,@Field(Params.PICTURE) String picture
            ,@Field(Params.DEVICE_TYPE) String device_type
            ,@Field(Params.DEVICE_TOKEN) String device_token
            ,@Field(Params.LOGIN_BY) String login_by);


    @FormUrlEncoded
    @POST(APIs.REGISTER)
    Call<String> register(@Field(Params.NAME) String name
            ,@Field(Params.EMAIL) String email
            ,@Field(Params.PASSWORD) String password
            ,@Field(Params.LOGIN_BY) String login_by
            ,@Field(Params.DEVICE_TYPE) String device_type
            , @Field(Params.DEVICE_TOKEN) String device_token);


    //ACCOUNT API

    @FormUrlEncoded
    @POST(APIs.LOGOUT)
    Call<String> logOutUser(@Field(Params.ID) int id
            , @Field(Params.TOKEN) String token);

    @FormUrlEncoded
    @POST(APIs.DELETE_ACCOUNT)
    Call<String> deleteuseraccount(@Field(Params.ID) int id
            ,@Field(Params.TOKEN) String token
            ,@Field(Params.PASSWORD) String password
    );

    //PASSWORD API

    @FormUrlEncoded
    @POST(APIs.FORGOT_PASSWORD)
    Call<String> forgotuserpassword(
            @Field(Params.EMAIL) String email);

    @FormUrlEncoded
    @POST(APIs.CHANGE_PASSWORD)
    Call<String> changeuserpassowrd(
            @Field(Params.ID) int id
    ,@Field(Params.TOKEN) String token
    ,@Field(Params.OLD_PASSWORD) String old_passowrd,
    @Field(Params.PASSWORD) String new_password
    ,@Field(Params.CONFIRM_PASSWORD) String confirm_password);

    //USER PROFILE API

    @FormUrlEncoded
    @POST(APIs.USER_PROFILE)
    Call<String> getUserProfile(
            @Field(Params.ID) int id,
            @Field(Params.TOKEN) String token);

    @Multipart
    @POST(APIs.UPDATE_PROFILE)
    Call<String> updateProfile(
            @Part(Params.ID) RequestBody id,
            @Part(Params.TOKEN) RequestBody token,
            @Part(Params.FIRST_NAME) RequestBody name,
            @Part(Params.LAST_NAME) RequestBody lastname,
            @Part(Params.EMAIL) RequestBody email,
            @Part(Params.MOBILE) RequestBody mobile,
            @Part(Params.DESCRIPTION) RequestBody description,
            @Part MultipartBody.Part picture);


    //POST MANAGEMENT

    @Multipart
    @POST(APIs.LIST_POST)
    Call<String> listPost(
            @Part(Params.ID) int id,
            @Part(Params.TOKEN) String token);

    @Multipart
    @POST(APIs.CREATE_POST)
    Call<String> createPost(
            @Part(Params.ID) int id,
            @Part(Params.TOKEN) String token,
            @Part MultipartBody.Part picture,
            @Part(Params.PLATFORM) String platform,
            @Part(Params.DESCRIPTION) String description,
            @Part(Params.USERACCOUNT_ID) int useraccountid);

    @Multipart
    @POST(APIs.POST_VIEW)
    Call<String> postView(
            @Part(Params.ID) int id,
            @Part(Params.TOKEN) String token,
            @Part(Params.POST_ID) int post_id);

    @Multipart
    @POST(APIs.DELETE_POST)
    Call<String> deletePost(
            @Part(Params.ID) int id,
            @Part(Params.TOKEN) String token,
            @Part(Params.POST_ID) int post_id);

    //Static Pages
    @GET(APIs.GET_STATIC_PAGE)
    Call<String> getStaticPage(@Query(Params.PAGE_TYPE) String page_type);

    //Facebook Pages
    @Multipart
    @POST(APIs.CREATE_USER_ACCOUNT)
    Call<String>  createUserAccount(
            @Part(Params.ID) int id,
            @Part(Params.TOKEN) String token,
            @Part(Params.PLATFORM) String platform,
            @Part(Params.ACCOUNTDATA) String accountdata);


    @FormUrlEncoded
    @POST(APIs.LIST_USER_ACCOUNTS)
    Call<String> listUserAccount(
            @Field(Params.ID) int id,
            @Field(Params.TOKEN) String token);

    @FormUrlEncoded
    @POST(APIs.DELETE_USER_ACCOUNT)
    Call<String> deleteUserAccount(
            @Field(Params.ID) int id,
            @Field(Params.TOKEN) String token,
            @Field(Params.USERACCOUNT_ID) int userAccountId);

}
