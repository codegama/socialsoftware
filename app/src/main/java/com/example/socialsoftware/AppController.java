package com.example.socialsoftware;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppController extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // Make sure we use vector drawables
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/rental_normal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
