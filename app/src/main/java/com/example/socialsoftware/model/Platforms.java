package com.example.socialsoftware.model;

public class Platforms {
    private  String platform;

    public Platforms(String platform) {
        this.platform = platform;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "Platforms{" +
                "platform=" + platform +
                //", platformImage =" + platformImage +
                '}';
    }

}
