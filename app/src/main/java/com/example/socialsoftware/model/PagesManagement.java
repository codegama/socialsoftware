package com.example.socialsoftware.model;

public class PagesManagement {
    private int useraccountid;
    private int userid;
    private String pagecategory;
    private int pageid;
    private String pagesname;
    private String pagesTypeplatform;
    private String pageaccessToken;
    private String useraccessToken;
    private String pagesimage;
    private String pagetypeImage;

    public PagesManagement(int useraccountid, int userid, String pagecategory, int pageid, String pagesname, String pagesTypeplatform, String pageaccessToken, String useraccessToken, String pagesimage, String pagetypeImage) {
        this.useraccountid = useraccountid;
        this.userid = userid;
        this.pagecategory = pagecategory;
        this.pageid = pageid;
        this.pagesname = pagesname;
        this.pagesTypeplatform = pagesTypeplatform;
        this.pageaccessToken = pageaccessToken;
        this.useraccessToken = useraccessToken;
        this.pagesimage = pagesimage;
        this.pagetypeImage = pagetypeImage;
    }

    public int getUseraccountid() {
        return useraccountid;
    }

    public void setUseraccountid(int useraccountid) {
        this.useraccountid = useraccountid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getPagecategory() {
        return pagecategory;
    }

    public void setPagecategory(String pagecategory) {
        this.pagecategory = pagecategory;
    }

    public int getPageid() {
        return pageid;
    }

    public void setPageid(int pageid) {
        this.pageid = pageid;
    }

    public String getPagesname() {
        return pagesname;
    }

    public void setPagesname(String pagesname) {
        this.pagesname = pagesname;
    }

    public String getPagesTypeplatform() {
        return pagesTypeplatform;
    }

    public void setPagesTypeplatform(String pagesTypeplatform) {
        this.pagesTypeplatform = pagesTypeplatform;
    }

    public String getPageaccessToken() {
        return pageaccessToken;
    }

    public void setPageaccessToken(String pageaccessToken) {
        this.pageaccessToken = pageaccessToken;
    }

    public String getUseraccessToken() {
        return useraccessToken;
    }

    public void setUseraccessToken(String useraccessToken) {
        this.useraccessToken = useraccessToken;
    }

    public String getPagesimage() {
        return pagesimage;
    }

    public void setPagesimage(String pagesimage) {
        this.pagesimage = pagesimage;
    }

    public String getPagetypeImage() {
        return pagetypeImage;
    }

    public void setPagetypeImage(String pagetypeImage) {
        this.pagetypeImage = pagetypeImage;
    }
    @Override
    public String toString() {
        return "PageAccounts{" +
                "userAccountId=" + useraccountid +
                ", userId='" + userid + '\'' +
                ", pageCategory='" + pagecategory + '\'' +
                ", pageId='" + pageid + '\'' +
                ", pagesName='" + pagesname +  '\'' +
                ", pagesTypePlatform='" + pagesTypeplatform +  '\'' +
                ", pagesImage='" + pagesimage +  '\'' +
                ", pageAccessToken='" + pageaccessToken +  '\'' +
                ", userAccessToken=" + useraccessToken +
                '}';
    }
}
