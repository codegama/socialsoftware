package com.example.socialsoftware.model;

import java.io.Serializable;

public class InstaSentPostRV implements Serializable {
    private int postid,postaccountid;
    private String platfrom;
    private int postgalleryid;
    private String postimage;
    private String publish;
    private String description;
    private String createdAt;

    public InstaSentPostRV(int postid, int postaccountid, String platfrom, int postgalleryid, String postimage, String publish, String description, String createdAt) {
        this.postid = postid;
        this.postaccountid = postaccountid;
        this.platfrom = platfrom;
        this.postgalleryid = postgalleryid;
        this.postimage = postimage;
        this.publish = publish;
        this.description = description;
        this.createdAt = createdAt;
    }


    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }


    public int getPostgalleryid() {
        return postgalleryid;
    }

    public void setPostgalleryid(int postgalleryid) {
        this.postgalleryid = postgalleryid;
    }

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public int getPostaccountid() {
        return postaccountid;
    }

    public void setPostaccountid(int postaccountid) {
        this.postaccountid = postaccountid;
    }

    public String getPlatfrom() {
        return platfrom;
    }

    public void setPlatfrom(String platfrom) {
        this.platfrom = platfrom;
    }

    public String getPostimage() {
        return postimage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPostimage(String postimage) {
        this.postimage = postimage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    @Override
    public String toString() {
        return "InstaSentPostRV{" +
                "postid=" + postid +
                ", postaccountid='" + postaccountid + '\'' +
                ", platfrom='" + platfrom + '\'' +
                ", postgalleryid='" + postgalleryid + '\'' +
                ", postimage='" + postimage +  '\'' +
                ", publish='" + publish +  '\'' +
                ", description='" + description +  '\'' +
                ", createAt=" + createdAt +
                '}';
    }
}
