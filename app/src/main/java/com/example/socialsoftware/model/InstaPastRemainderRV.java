package com.example.socialsoftware.model;

public class InstaPastRemainderRV {

    int  remainderimage,remaiderpostimage,repostimage;
    String remaidertext,posttoinsta,pastremaintime,postdate;

    public InstaPastRemainderRV(int remainderimage, int remaiderpostimage, int repostimage, String remaidertext, String posttoinsta, String pastremaintime, String postdate) {
        this.remainderimage = remainderimage;
        this.remaiderpostimage = remaiderpostimage;
        this.repostimage = repostimage;
        this.remaidertext = remaidertext;
        this.posttoinsta = posttoinsta;
        this.pastremaintime = pastremaintime;
        this.postdate = postdate;
    }

    public int getRemainderimage() {
        return remainderimage;
    }

    public void setRemainderimage(int remainderimage) {
        this.remainderimage = remainderimage;
    }

    public int getRemaiderpostimage() {
        return remaiderpostimage;
    }

    public void setRemaiderpostimage(int remaiderpostimage) {
        this.remaiderpostimage = remaiderpostimage;
    }

    public int getRepostimage() {
        return repostimage;
    }

    public void setRepostimage(int repostimage) {
        this.repostimage = repostimage;
    }

    public String getRemaidertext() {
        return remaidertext;
    }

    public void setRemaidertext(String remaidertext) {
        this.remaidertext = remaidertext;
    }

    public String getPosttoinsta() {
        return posttoinsta;
    }

    public void setPosttoinsta(String posttoinsta) {
        this.posttoinsta = posttoinsta;
    }

    public String getPastremaintime() {
        return pastremaintime;
    }

    public void setPastremaintime(String pastremaintime) {
        this.pastremaintime = pastremaintime;
    }

    public String getPostdata() {
        return postdate;
    }

    public void setPostdata(String postdate) {
        this.postdate = postdate;
    }
}
