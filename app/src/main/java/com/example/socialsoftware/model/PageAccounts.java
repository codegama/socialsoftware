package com.example.socialsoftware.model;

public class PageAccounts {
    private int userAccountId;
    private int userId;
    private String pageCategory;
    private int pageId;
    private String pagesName;
    private String pagesTypePlatform;
    private String pageAccessToken;
    private String userAccessToken;
    private String pagesImage;
    private String pageTypeImage;

    public PageAccounts(int userAccountId, int userId, String pageCategory, int pageId, String pagesName, String pagesTypePlatform, String pageAccessToken, String userAccessToken, String pagesImage, String pageTypeImage) {
        this.userAccountId = userAccountId;
        this.userId = userId;
        this.pageCategory = pageCategory;
        this.pageId = pageId;
        this.pagesName = pagesName;
        this.pagesTypePlatform = pagesTypePlatform;
        this.pageAccessToken = pageAccessToken;
        this.userAccessToken = userAccessToken;
        this.pagesImage = pagesImage;
        this.pageTypeImage = pageTypeImage;
    }


    public int getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(int userAccountId) {
        this.userAccountId = userAccountId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPageCategory() {
        return pageCategory;
    }

    public void setPageCategory(String pageCategory) {
        this.pageCategory = pageCategory;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getPagesName() {
        return pagesName;
    }

    public void setPagesName(String pagesName) {
        this.pagesName = pagesName;
    }

    public String getPagesTypePlatform() {
        return pagesTypePlatform;
    }

    public void setPagesTypePlatform(String pagesTypePlatform) {
        this.pagesTypePlatform = pagesTypePlatform;
    }

    public String getPageAccessToken() {
        return pageAccessToken;
    }

    public void setPageAccessToken(String pageAccessToken) {
        this.pageAccessToken = pageAccessToken;
    }

    public String getUserAccessToken() {
        return userAccessToken;
    }

    public void setUserAccessToken(String userAccessToken) {
        this.userAccessToken = userAccessToken;
    }

    public String getPagesImage() {
        return pagesImage;
    }

    public void setPagesImage(String pagesImage) {
        this.pagesImage = pagesImage;
    }

    public String getPageTypeImage() {
        return pageTypeImage;
    }

    public void setPageTypeImage(String pageTypeImage) {
        this.pageTypeImage = pageTypeImage;
    }


    @Override
    public String toString() {
        return "PageAccounts{" +
                "userAccountId=" + userAccountId +
                ", userId='" + userId + '\'' +
                ", pageCategory='" + pageCategory + '\'' +
                ", pageId='" + pageId + '\'' +
                ", pagesName='" + pagesName +  '\'' +
                ", pagesTypePlatform='" + pagesTypePlatform +  '\'' +
                ", pageAccessToken='" + pageAccessToken +  '\'' +
                ", userAccessToken=" + userAccessToken +
                '}';
    }


}
